﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class PigeonMayo
{
    static PigeonMayo()
    {
        EditorApplication.playModeStateChanged += OnPlayModeChanged;
    }

    [MenuItem("PigeonMayo/Application/Play Game _F5")]
    private static void PlayGame()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

            EditorApplication.EnterPlaymode();
        }
        else
            EditorApplication.ExitPlaymode();
    }
    
    [MenuItem("PigeonMayo/Application/Pause Game _F6")]
    private static void PauseGame()
    {
        EditorApplication.isPaused = !EditorApplication.isPaused;
    }
    
    [MenuItem("PigeonMayo/Application/Stop Game _F7")]
    private static void StopGame()
    {
        EditorApplication.ExitPlaymode();
    }

    [MenuItem("PigeonMayo/Init Project")]
    public static void InitProject()
    {
        CreateFolder("Scripts");
        CreateFolder("Prefabs");
        CreateFolder("Materials");
        CreateFolder("Animation");
        CreateFolder("Texture");
        CreateFolder("UI");
        CreateFolder("Resources");
    }
    
    private static void CreateFolder(string _folder)
    {
        string path = $"Assets/{_folder}";
      
        if (!AssetDatabase.IsValidFolder(path))
            AssetDatabase.CreateFolder("Assets",_folder);
        else
            Debug.LogWarning($"Folder {_folder} already exists");

    }

    [MenuItem("PigeonMayo/Screenshot _F9")]
    private static void Screenshot()
    {
        if (!Directory.Exists(Application.dataPath+"/Screenshots"))
            Directory.CreateDirectory(Application.dataPath + "/Screenshots");
        
        string path = Application.dataPath+"/Screenshots/screenshot";
        
        int count = 1;
        while(File.Exists($"{path}.png"))
            path = $"{path}({count++})";

        ScreenCapture.CaptureScreenshot($"{path}.png");
        AssetDatabase.Refresh();

    }

    [MenuItem("PigeonMayo/Create Scene")]
    private static void CreateNewScene()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        
        Scene scene = CreateAndSaveScene();
        
        AddToBuildsSetting(scene);
    }
    
    private static Scene CreateAndSaveScene()
    {
        NewSceneSetup preset = NewSceneSetup.EmptyScene;
        NewSceneMode mode = NewSceneMode.Single;
        Scene scene = EditorSceneManager.NewScene(preset, mode);

        string folder = "Scenes";
        string sceneName = "New Scene";
        string path = $"Assets/Projet/{folder}/";
        string fileName = $"{sceneName}.unity";
        string file = $"{path}{fileName}";
        
        int count = 1;
        while(File.Exists(file))
        {
            file = $"{path}{sceneName}({count}).unity";
            count++;
        }
        
        EditorSceneManager.SaveScene(scene, $"{path}{fileName}");
        return scene;
    }
    
    private static void AddToBuildsSetting(Scene scene)
    {
        List<EditorBuildSettingsScene> buildScene = new List<EditorBuildSettingsScene>();
        foreach (EditorBuildSettingsScene item in EditorBuildSettings.scenes)
        {
            buildScene.Add(item);
        }

        buildScene.Add(new EditorBuildSettingsScene(scene.path, true));
        EditorBuildSettings.scenes = buildScene.ToArray();
    }
    
    private static string SavedScenePath
    {
        get
        {
            return EditorPrefs.GetString("oldScenePath", string.Empty);
        }

        set
        {
            EditorPrefs.SetString("oldScenePath", value);
        }
    }
    
    private static void OnPlayModeChanged(PlayModeStateChange state)
    {
        if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
        {
            string currentScene = SceneManager.GetActiveScene().path;
            string firstScene = EditorBuildSettings.scenes[0].path;

            if (currentScene != firstScene)
            {
                int tResult = EditorUtility.DisplayDialogComplex("Entering Play Mode",
                        "Save Scene and Play from First Scene ?", "Yes", "No", "Cancel");

                switch (tResult)
                {
                    case 0:
                        EditorSceneManager.SaveOpenScenes();
                        SavedScenePath = currentScene;
                        EditorSceneManager.OpenScene(firstScene);
                        break;

                    case 1:
                        SavedScenePath = string.Empty;
                        break;

                    case 2:
                        SavedScenePath = string.Empty;
                        EditorApplication.isPlaying = false;
                        break;
                }
            }
            else
            {
                SavedScenePath = string.Empty;
            }
        }
        else if (!EditorApplication.isPlaying)
        {
            if (!string.IsNullOrEmpty(SavedScenePath))
            {
                EditorSceneManager.OpenScene(SavedScenePath);
            }
        }
    }
} 
