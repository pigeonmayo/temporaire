﻿using UnityEngine;

public abstract class State : ScriptableObject
{
    protected StateMachine m_stateMachine     = default;

    public void SetStateMachine(StateMachine _stateMachine)
    {
        m_stateMachine = _stateMachine;
    }

    public abstract void OnEnterState();

    public abstract void OnUpdateState();

    public abstract void OnExitState();
}
