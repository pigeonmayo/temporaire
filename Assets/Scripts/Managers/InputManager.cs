using System;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// All ActionMap for each type of playStyle
/// </summary>
public enum ActionMap{PlayerGround,UI,PlayerSwim}

/// <summary>
/// Manage all inputs in this manager. Send value and call corresponding method to apply the input.
/// Change <see cref="ActionMap"/> to change the controls mapping.
/// Rebind a input. 
/// <seealso cref="PlayerInput"/>
/// <seealso cref="InputActionAsset"/>
/// </summary>
public class InputManager : MonoBehaviour
{
   [SerializeField] private SerializableDictionary<string, Element> m_elements   = default;
   private InputActionAsset m_inputAssets                                        = default;
   private PlayerInput m_playerInput                                             = default;
   private ActionMap m_lastActionMap                                             = default;

   private void Awake()
   {
      m_playerInput = GetComponent<PlayerInput>();
      m_inputAssets = m_playerInput.actions;
      
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
   }
   
   /// <summary>
   /// Change <see cref="ActionMap"/> to change the controls.
   /// </summary>
   /// <param name="_actionMap">The new controls map</param>
   public void ChangeActionMap(ActionMap _actionMap)
   {
      m_playerInput.SwitchCurrentActionMap(_actionMap.ToString());
   }
   
   /// <summary>
   /// Change an Input key to another.
   /// </summary>
   /// <param name="_actionMap">The <see cref="ActionMap"/></param>
   /// <param name="_actionID">The action to change Input</param>
   public void RebindAction(ActionMap _actionMap,string _actionID)
   {
      m_playerInput.DeactivateInput();
      
      InputAction inputAction = m_inputAssets.FindAction(_actionID);
      var rebindOperation = inputAction.PerformInteractiveRebinding().OnComplete(RebindComplete).Start();
   }

   /// <summary>
   /// Close to rebinding operation when completed
   /// </summary>
   private void RebindComplete(InputActionRebindingExtensions.RebindingOperation _rebinding)
   {
      _rebinding.Dispose();
      m_playerInput.ActivateInput();
   }

   /// <summary>
   /// Get the current <see cref="ActionMap"/>
   /// </summary>
   public ActionMap CurrentMap()
   {
      return (ActionMap)Enum.Parse(typeof(ActionMap), m_playerInput.currentActionMap.name);
   }

   /// <summary>
   /// Check if the <see cref="Player"/> can enter inputs.
   /// Ex: The <see cref="Player"/> can't enter inputs while the <see cref="Camera"/> is animating.
   /// </summary>
   public bool CanEnterInput()
   {
      return !Mushboi.Instance.CameraController?.IsTransferring()??false;
   }

   #region Actions ==============================================

   /// <summary>
   /// Called when the defend button is pressed
   /// </summary>
   /// <param name="_value">The input value if it's pressed or not</param>
   private void OnDefend(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.Defend,_value.isPressed);
   }

   /// <summary>
   /// Called when the LockTarget button is pressed
   /// </summary>
   private void OnLockTarget()
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.LockTarget);
   }
   
   /// <summary>
   /// Called when the CycleLock buttons are pressed
   /// </summary>
   /// <param name="_value">The input value if it's positive(Right) or negative(Left)</param>
   private void OnCycleTarget(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.CycleTarget,_value.Get<float>());
   }
   
   /// <summary>
   /// Called when the Jump button is pressed
   /// </summary>
   /// <param name="_value">The input value if it's pressed or not</param>
   private void OnJump(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.Jump,_value.isPressed);
   }
   
   /// <summary>
   /// Called when the Attack button is pressed
   /// </summary>
   /// <param name="_value">The input value if it's pressed or not</param>
   private void OnAttack(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.Attack,_value.isPressed);
   }

   /// <summary>
   /// Called when the Interact button is pressed
   /// </summary>
   private void OnInteract()
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.Interact);
   }

   private void OnGroundPound(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.GroundPound,_value.isPressed);
   }

   private void OnTurnPage(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.TurnPage,_value.Get<float>());
   }

   /// <summary>
   /// Called when the RotateCamera Axis is Changed
   /// </summary>
   /// <param name="_value">The input axis</param>
   private void OnRotateCamera(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.RotateCamera,_value.Get<Vector2>());
   }
   
   /// <summary>
   /// Called when the Walk Axis is Changed
   /// </summary>
   /// <param name="_value">The input axis</param>
   private void OnWalk(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.Walk,_value.Get<Vector2>());
   }

   /// <summary>
   /// Called when the Sprint button is pressed
   /// </summary>
   /// <param name="_value">The input value if it's pressed or not</param>
   private void OnSprint(InputValue _value)
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.Sprint,_value.isPressed);
   }

   /// <summary>
   /// Called when the Look Forward button is pressed
   /// </summary>
   private void OnLookForward()
   {
      if(CanEnterInput())
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.LookForward);
   }

   /// <summary>
   /// Called when the Menu button is pressed
   /// </summary>
   private void OnMenuToggle()
   {
      if (CanEnterInput())
      {
         ActionMap currentMap = CurrentMap();
         if (currentMap == ActionMap.UI)
         {
            ChangeActionMap(m_lastActionMap);
         }
         else
         {
            ChangeActionMap(ActionMap.UI);
            m_lastActionMap = currentMap;
         }
         
         Mushboi.Instance.PauseGame();
      }
   }

   /// <summary>
   /// Called when the Cast Spell button is pressed
   /// </summary>
   private void OnCastSpell(InputValue _value)
   {
      if (CanEnterInput())
      {
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.CastSpell,_value.isPressed);
      }
   }

   /// <summary>
   /// Called when the Fire Spell button is pressed
   /// </summary>
   private void OnFireSpell()
   {
      const string FIRE = "Fire";
      
      if (CanEnterInput())
      {
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.ChangingElement,m_elements[FIRE]);
      }
   }
   
   /// <summary>
   /// Called when the Water Spell button is pressed
   /// </summary>
   private void OnWaterSpell()
   {
      const string WATER = "Water";
      
      if (CanEnterInput())
      {
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.ChangingElement,m_elements[WATER]);
      }
   }
   
   /// <summary>
   /// Called when the Wind Spell button is pressed
   /// </summary>
   private void OnWindSpell()
   {
      const string WIND = "Wind";
      
      if (CanEnterInput())
      {
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.ChangingElement,m_elements[WIND]);
      }
   }
   
   /// <summary>
   /// Called when the Poison Spell button is pressed
   /// </summary>
   private void OnPoisonSpell()
   {
      const string POISON = "Poison";
      
      if (CanEnterInput())
      {
         Mushboi.Instance.EventManager.BroadcastEvent(EventID.ChangingElement,m_elements[POISON]);
      }
   }

   #endregion
}
