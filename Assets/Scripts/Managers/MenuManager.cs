using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
   [SerializeField] private string m_firstScene;

   public void Start()
   {
      Mushboi.Instance.InputManager.ChangeActionMap(ActionMap.UI);
      
      Cursor.lockState = CursorLockMode.Confined;
      Cursor.visible = true;
   }
   
   public void NewGame()
   {
      const string PLAYER_SCENE = "Player";
      
      Transition.Instance.ChangeScene(m_firstScene, () =>
      {
         SceneManager.LoadSceneAsync(PLAYER_SCENE, LoadSceneMode.Additive);
         Mushboi.Instance.AddActiveMap(m_firstScene);
         Mushboi.Instance.InputManager.ChangeActionMap(ActionMap.PlayerGround);
      });

   }

   public void LoadGame()
   {
      
   }

   public void Options()
   {
      
   }

   public void QuitGame()
   {
      Transition.Instance.FadeQuit();
   }
}
