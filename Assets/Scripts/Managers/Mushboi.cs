using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// States to game can be in.
/// </summary>
/// <seealso cref="Mushboi"/>
public enum GameState
{
    Play,
    Menu,
    Cinematic,
    Loading
}

/// <summary>
/// Regroup all managers and control the game.
/// <example>
///     Use <see cref="InputManager"/> with :
///     <c>Mushboi.Instance.InputManager.ChangeActionMap("UI");</c>
/// </example>
/// </summary>
/// <seealso cref="EventManager"/>
/// <seealso cref="PoolingManager"/>
/// <seealso cref="CameraController"/>
public class Mushboi : Singleton<Mushboi>
{
    [Header("Managers")]
    [SerializeField] private InputManager m_inputManager        = default;
    [SerializeField] private EventManager m_eventManager        = default;
    [SerializeField] private PoolingManager m_poolingManager    = default;
    
    [Header("Reference")]
    [SerializeField] private Player m_player                    = default;
    [SerializeField] private Firefly m_firefly                  = default;
    [SerializeField] private CameraController m_camController   = default;
    private GameState m_currentState                            = default;
    private List<string> m_activesMap                           = new List<string>();
    
    //Getter
    public List<string> ActivesMap                              => m_activesMap;
    public InputManager InputManager                            => m_inputManager;
    public EventManager EventManager                            => m_eventManager;
    public PoolingManager PoolingManager                        => m_poolingManager;
    public Player Player                                        => m_player;
    public Firefly Firefly                                      => m_firefly;
    public CameraController CameraController                    => m_camController;
    public GameState CurrentGameState                           => m_currentState;

    /// <summary>
    /// Set the <see cref="Player"/> in the game
    /// </summary>
    public void SetPlayer(Player _player)
    {
        m_player = _player;
    }
    
    /// <summary>
    /// Set the <see cref="Firefly"/> in the game
    /// </summary>
    public void SetFirefly(Firefly _firefly)
    {
        m_firefly = _firefly;
    }

    /// <summary>
    /// Set the <see cref="CameraController"/> of the main <see cref="Camera"/> in the game
    /// </summary>
    public void SetCamera(CameraController _cam)
    {
        m_camController = _cam;
    }

    /// <summary>
    /// Add a <see cref="Scene"/> in the list of loaded <see cref="Scene"/>.
    /// </summary>
    /// <param name="_scene">The new <see cref="Scene"/> to be added.</param>
    public void AddActiveMap(string _scene)
    {
        m_activesMap.Add(_scene);
    }

    /// <summary>
    /// Remove a <see cref="Scene"/> in the list of loaded <see cref="Scene"/>.
    /// </summary>
    /// <param name="_scene">The new <see cref="Scene"/> to be removed.</param>
    public void RemoveActiveMap(string _scene)
    {
        m_activesMap.Remove(_scene);
    }

    /// <summary>
    /// Call this function to save the game.
    /// It'll call all necessary function to obtain all information to save.
    /// </summary>
    public void SaveGame()
    {
        m_eventManager.BroadcastEvent(EventID.SaveGame);
    }

    /// <summary>
    /// Call this function to load the game.
    /// It'll call all necessary function to send all information to load.
    /// </summary>
    public void LoadGame()
    {
        m_eventManager.BroadcastEvent(EventID.LoadGame);
    }

    /// <summary>
    /// Call this function to pause the game.
    /// It'll call all necessary function to pause.
    /// </summary>
    public void PauseGame()
    {
        if (m_currentState == GameState.Play)
        {
            m_currentState = GameState.Menu;
            //TODO:pause stuff
            m_eventManager.BroadcastEvent(EventID.PauseGame);
        }
        else
        {
            m_currentState = GameState.Play;
            //TODO:resume stuff
            m_eventManager.BroadcastEvent(EventID.ResumeGame);
        }
        
    }
}
