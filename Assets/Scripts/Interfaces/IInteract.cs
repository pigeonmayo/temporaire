﻿/// <summary>
/// Inherit <see cref="IInteract"/> to be able to interact with objects or the creatures.
/// </summary>
public interface IInteract
{
    public void Interact();
}