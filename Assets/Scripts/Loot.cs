using UnityEngine;

public class Loot : PoolItem
{
    private enum LootType
    {
        Money,
        HealthOrb,
        ManaOrb,
    }

    [SerializeField] private LootType m_type    = default;
    [SerializeField] private int m_amount       = default;
    private Rigidbody m_rigidbody               = default;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    public void SpawnLoot()
    {
        const float LOOT_FORCE = 3;
        
        Vector2 xDir = Random.insideUnitCircle;
        Vector3 dir = Vector3.up + new Vector3(xDir.x,0,xDir.y);

        m_rigidbody.constraints = RigidbodyConstraints.None;
        m_rigidbody.AddForce(dir*LOOT_FORCE,ForceMode.Impulse);
    }

    public void OnTriggerEnter(Collider _other)
    {
        Player player = _other.GetComponent<Player>();
        
        if (player)
        {
            Mushboi.Instance.Player.AddMoney(m_amount);
            Deactivate();
        }
    }

    public void OnCollisionEnter(Collision _other)
    {
        if (_other.gameObject.IsInLayerMask(LayerMask.GetMask("Ground")))
        {
            m_rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            m_rigidbody.velocity = Vector3.zero;
        }
    }
}
