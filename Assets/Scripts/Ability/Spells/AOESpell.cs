using UnityEngine;

[CreateAssetMenu(fileName = "AOE Spell",menuName = "Spells/AOE Spell")]
public class AOESpell : Spell
{
    [Header("AOE")]
    [SerializeField] private GameObject m_aoe           = default;
    [SerializeField] private LayerMask m_layerMask      = default;
    [SerializeField] private bool m_isFollowing         = default;
    
    public override void ReleaseSpell()
    {
        if (m_holding && Mushboi.Instance.Player.PlayerController.IsGrounded)
        {
            Vector3 pos = Mushboi.Instance.Player.transform.position;
            AOE aoe = Mushboi.Instance.PoolingManager.SpawnItem(m_aoe).GetComponent<AOE>();
            aoe.SetAOE(m_damage,m_lifeTime,m_element,m_layerMask,m_isFollowing?Mushboi.Instance.Player.transform:null);
            aoe.SetPosition(pos);
            base.ReleaseSpell();
        }
    }
}
