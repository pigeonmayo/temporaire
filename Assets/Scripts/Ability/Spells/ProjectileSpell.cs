﻿using UnityEngine;

[CreateAssetMenu(fileName = "ProjectileSpell",menuName = "Spells/ProjectileSpell")]
public class ProjectileSpell : Spell
{
    [SerializeField] private GameObject m_projectile    = default;
    [SerializeField] private LayerMask m_layerMask      = default;
    
    public override void ReleaseSpell()
    {
        if (m_holding)
        {
            Vector3 direction = default;
            if (Mushboi.Instance.Firefly.GetTarget())
                direction = Mushboi.Instance.Firefly.GetTarget().position - Mushboi.Instance.Player.transform.position;
            else
                direction = Mushboi.Instance.Player.transform.forward;    
            
            Projectile projectile = Mushboi.Instance.PoolingManager.SpawnItem(m_projectile).GetComponent<Projectile>();
            projectile.SetProjectile(m_damage,m_lifeTime,m_element,direction.normalized*m_projectileSpeed,m_layerMask);
            projectile.SetPosition(Mushboi.Instance.Player.GetShootingPosition());
            base.ReleaseSpell();
        }
    }
}
