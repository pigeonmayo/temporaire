using UnityEngine;

[CreateAssetMenu(fileName = "Windpush",menuName = "Spells/Windpush")]
public class Windpush : Spell
{
    public override void ReleaseSpell()
    {
        if (m_holding)
        {
            Debug.Log($"Windpush : {m_holdingTimer}");
            base.ReleaseSpell();
        }
    }
}
