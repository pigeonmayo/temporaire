using UnityEngine;

public abstract class Spell : ScriptableObject
{
    #region Enum
    public enum DamageType {Damage,Status}
    #endregion
    
    protected float m_cooldownTimer         = 0;
    protected bool m_activated              = default;
    protected bool m_holding                = default;
    protected float m_holdingTimer          = default;
    
    [Header("Spell")]
    public Element m_element                = default;
    public int m_damage                     = default;
    public float m_cooldown                 = default;
    public float m_lifeTime                 = default;
    public int m_manaCost                   = 0;
    public DamageType m_damageType          = default;
    public float m_range                    = default;
    public float m_projectileSpeed          = default;
    [TextArea] public string m_description  = default;

    public void Awake()
    {
        m_holdingTimer = 0;
        m_holding = false;
        m_activated = false;
        m_cooldownTimer = 0;
    }

    /// <summary>
    /// Called when the button castSpell is pressed.
    /// This function will start the timer.
    /// </summary>
    public virtual void HoldSpell()
    {
        if (m_activated)
            return;
        
        m_holding = true;
    }

    /// <summary>
    /// Called when the button castSpell is released.
    /// This function will cast the <see cref="Spell"/>.
    /// </summary>
    public virtual void ReleaseSpell()
    {
        m_holdingTimer = 0;
        m_holding = false;
        m_activated = true;
        m_cooldownTimer = m_cooldown;
    }

    /// <summary>
    /// Update the <see cref="Spell"/> if needed.
    /// Update the timer and cooldown.
    /// </summary>
    public virtual void UpdateSpell()
    {
        if (m_activated)
        {
            m_cooldownTimer -= Time.deltaTime;
            if (m_cooldownTimer <= 0)
                m_activated = false;
        }
        else
        {
            if (m_holding)
                m_holdingTimer += Time.deltaTime;
        }
    }
}
