using UnityEngine;

public abstract class Skill : ScriptableObject
{
    protected Player m_player                   = default;
    protected float m_cooldownTimer             = 0;
    protected bool m_activated                  = default;

    [Header("Skill")] 
    public float m_cooldown                     = default;
    public Skill m_upgrade                      = default;
    [TextArea] public string m_description      = default;

    /// <summary>
    /// Load the <see cref="Skill"/> at the first time in the <see cref="Player"/> skills.
    /// </summary>
    public virtual void Load()
    {
        m_player = Mushboi.Instance.Player;
    }

    /// <summary>
    /// Unload the <see cref="Skill"/>.
    /// Ex: Remove an event in the <see cref="EventManager"/>
    /// </summary>
    public virtual void Unload()
    {
    }

    /// <summary>
    /// Activate the <see cref="Skill"/> when the condition is achieved.
    /// Ex: JumpButton is pressed in the air.
    /// </summary>
    public abstract void ActivateSkill(object _inputValue);

    /// <summary>
    /// Update the skill if needed.
    /// Ex: Update the cooldown.
    /// </summary>
    public virtual void UpdateSkill()
    {
        if (m_activated)
        {
            m_cooldownTimer -= Time.deltaTime;
            if (m_cooldownTimer <= 0)
                m_activated = false;
        }
    }
}