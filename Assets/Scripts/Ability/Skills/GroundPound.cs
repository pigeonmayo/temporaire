﻿using UnityEngine;

[CreateAssetMenu(fileName = "GroundPound",menuName = "Skills/GroundPound")]
public class GroundPound : Skill
{
    [Header("GroundPound")]
    [SerializeField] private float m_poundForce     = default;
    [SerializeField] private float m_poundRadius    = default;
    [SerializeField] private int m_poundDamage      = default;
    [SerializeField] private GameObject m_effect    = default;
    private Rigidbody m_rigidbody                   = default;
    
    public override void Load()
    {
        base.Load();
        
        Mushboi.Instance.EventManager.RegisterEvent(EventID.GroundPound,ActivateSkill);
        m_rigidbody = m_player.GetComponent<Rigidbody>();
    }

    public override void Unload()
    {
        if(!Mushboi.m_isClosing)
            Mushboi.Instance.EventManager.UnregisterEvent(EventID.GroundPound,ActivateSkill);  
    }

    public override void ActivateSkill(object _inputPressed)
    {
        if ((bool)_inputPressed)
        {
            if (!m_activated && !Mushboi.Instance.Player.PlayerController.IsGrounded)
            {
                m_activated = true;
                m_rigidbody.velocity = Vector3.zero;
                m_rigidbody.AddForce(Vector3.down*m_poundForce,ForceMode.Impulse);
            }
        }
    }

    public override void UpdateSkill()
    {
        if (m_activated && Mushboi.Instance.Player.PlayerController.IsGrounded)
        {
            m_activated = false;
            DoDamage(); 
        }
    }
    
    /// <summary>
    /// Do damage to all entity in range when player use his skill and touch the ground.
    /// </summary>
    public void DoDamage()
    {
        Vector3 pos = Mushboi.Instance.Player.transform.position;
        Mushboi.Instance.PoolingManager.SpawnItem(m_effect).SetPosition(pos);
        
        Collider[] colliders = Physics.OverlapSphere(pos,m_poundRadius);
        foreach (Collider c in colliders)
        {
            if (c.gameObject == m_player.gameObject)
                continue;
            
            if(c.TryGetComponent(out Entity entity))
                entity.TakeDamage(m_poundDamage);
        }
    }
}
