using UnityEngine;

[CreateAssetMenu(fileName = "Double Jump",menuName = "Skills/DoubleJump")]
public class DoubleJump : Skill
{
    [Header("Double Jump")]
    [SerializeField] private float m_doubleJumpForce    = default;
    private Rigidbody m_rigidbody                       = default;
    
    public override void Load()
    {
        base.Load();
        
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Jump,ActivateSkill);
        m_rigidbody = m_player.GetComponent<Rigidbody>();
    }

    public override void Unload()
    {
        if(!Mushboi.m_isClosing)
            Mushboi.Instance.EventManager.UnregisterEvent(EventID.Jump,ActivateSkill);  
    }

    public override void ActivateSkill(object _inputPressed)
    {
        if ((bool)_inputPressed)
        {
            if (!m_activated && !Mushboi.Instance.Player.PlayerController.IsGrounded)
            {
                m_activated = true;
                Mushboi.Instance.Player.PlayerController.Jump();
            }
        }
    }

    public override void UpdateSkill()
    {
        if (m_activated && Mushboi.Instance.Player.PlayerController.IsGrounded)
        {
            m_activated = false;
        }
    }
}
