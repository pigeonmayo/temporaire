using UnityEngine;

[CreateAssetMenu(fileName = "new Element",menuName = "Elements")]
public class Element : ScriptableObject
{
    public Color m_elementColor     = default;
    public Sprite m_elementIcon     = default;
}
