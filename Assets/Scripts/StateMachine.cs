using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [SerializeField] private SerializableDictionary<string,State> m_states  = new SerializableDictionary<string, State>();
    [SerializeField] private State m_defaultState                           = default; 
    private State m_currentState                                            = default;
    
    public State CurrentState                                               => m_currentState;
    
    private void Awake()
    {
        foreach (var key in m_states.Keys)
            m_states[key].SetStateMachine(this);

        ResetStateMachine();
    }

    private void Update()
    {
        m_currentState?.OnUpdateState();
    }

    public void ChangeState(string _stateName)
    {
        ChangeState(m_states[_stateName]);
    }

    public void ChangeState(State _state)
    {
        m_currentState?.OnExitState();
        m_currentState = _state;
        m_currentState.OnEnterState();
    }

    public void ResetStateMachine()
    {
        ChangeState(m_defaultState);
    }

}
