using System.Collections;
using UnityEngine;

/// <summary>
/// Controls the CameraRig on which the Main <see cref="Camera"/> is attached.
/// Manages the Lock functionality of the camera as well as the various rotations.
/// </summary>
public class CameraController : MonoBehaviour
{
    private Transform m_target      = default;
    private Transform m_lastTarget  = default;

    [Header("Rotation")]    
    [SerializeField,Range(1f,100f)] private float m_yawSpeed            = 1f;
    [SerializeField,Range(1f,100f)] private float m_pitchSpeed          = 1f;
    [SerializeField, Range(1f, 360f)] private float m_LockRotateSpeed   = 180f;
    [SerializeField] private bool m_isInvertedX                         = false;
    [SerializeField] private bool m_isInvertedY                         = true;
    private Vector2 m_rotation                                          = default;
    private Transform m_camera                                          = default;

    [Header("Menu")]
    [SerializeField] private Transform m_menuCameraTransformPosition;
    [SerializeField] private float m_menuTransitionTime;
    private Pivot m_playCameraPosition;
    private Pivot m_menuCameraPosition;
    private Coroutine m_toggleRoutine;
    private bool m_isTransferring                                         = false;

    private void OnEnable()
    {
        if (Mushboi.m_isClosing)
            return;
        
        Mushboi.Instance.EventManager.RegisterEvent(EventID.PauseGame,ToMenu);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.RotateCamera,SetRotation);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.ResumeGame, ToPlay);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.LookForward, LookForward);
    }
    
    private void OnDisable()
    {
        if (Mushboi.m_isClosing) //if the game is closing and the game instance has been destroyed already, return.
            return;
            
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.PauseGame,ToMenu);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.RotateCamera,SetRotation);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.ResumeGame, ToPlay);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.LookForward, LookForward);

    }
    
    private void Awake()
    {
        m_camera = GetComponentInChildren<Camera>().transform;
        
        m_playCameraPosition = new Pivot(m_camera);
        m_menuCameraPosition = new Pivot(m_menuCameraTransformPosition);
    }

    private void Start()
    {
        //Set the CameraController object of the current game instance to this CameraController.
        Mushboi.Instance.SetCamera(this);
    }

    private void LateUpdate()
    {
        Rotate();
    }

    /// <summary>
    /// Updates the rotation of the rig and the camera to the current target.
    /// </summary>
    private void UpdateLock()
    {
        Vector3 targetPos = m_target.position;
        Vector3 cameraVector = targetPos - m_camera.position;

        targetPos.y = transform.position.y; //brings the y portion of the target to the same y level to limit the rotations to the Y axis.
        Vector3 rigVector = targetPos - transform.position;

        float singleStep = m_LockRotateSpeed * Mathf.Deg2Rad * Time.deltaTime;
        Vector3 lookDirection = Vector3.RotateTowards(transform.forward, rigVector, singleStep, 0.0f);
        transform.rotation = Quaternion.LookRotation(lookDirection);

        //TODO:try to find a better way to lerp the camera smoothly to the target instead of snapping it. Quaternion.Lerp() could do the trick with Quaternion.FromToRotation()
        m_camera.LookAt(m_target);
    }

    /// <summary>
    /// Updates the position of the CameraRig based on the transform of the player. Updated the rotations if locked on a target.
    /// </summary>
    /// <param name="_player">The Transform of the player</param>
    public void UpdateCameraRig(Transform _player)
    {
        transform.position = _player.position;
        
        if (m_target && !m_isTransferring)
        {
            UpdateLock();
        }
    }
    
    /// <summary>
    /// Rotate the camera rig to look in front of the <see cref="Player"/>.
    /// </summary>
    private void LookForward(object _inputPressed = null)
    {
        transform.forward = Mushboi.Instance.Player.transform.forward;
    }
    
    /// <summary>
    /// Sets the m_target to the _target Transform
    /// </summary>
    /// <param name="_target">The Transform of the target to lock on</param>
    public void LockTarget(Transform _target)
    {
        m_target = _target;
        m_lastTarget = null;
    }

    /// <summary>
    /// Locks target using a GameObject
    /// </summary>
    /// <see cref="LockTarget(UnityEngine.Transform)"/>
    /// <param name="_target">The GameObject to lock on</param>
    public void LockTarget(GameObject _target)
    {
        LockTarget(_target?.transform);
    }

    /// <summary>
    /// Removes the current lock target
    /// </summary>
    public void UnlockTarget()
    {
        m_target = null;
    }

    /// <summary>
    /// Rotates the rig on the y axis and the camera on the X axis based on the inputs received
    /// </summary>
    private void Rotate()
    {
        transform.Rotate(Vector3.up,m_rotation.x*(m_isInvertedX?-m_yawSpeed:m_yawSpeed)*Time.deltaTime);
        m_camera.Rotate(Vector3.right, m_rotation.y * (m_isInvertedY ? -m_pitchSpeed : m_pitchSpeed) * Time.deltaTime);
        
        //After the camera rotated, make sure the angle stays within a certain threshold
        Vector3 euler = m_camera.eulerAngles;
        if (euler.x > 180f)
        {
            if (euler.x < 340f)
                euler.x = 340f;
        }
        else
        {
            if (euler.x > 20f)
                euler.x = 20f;
        }
        
        m_camera.eulerAngles = euler;
    }

    /// <summary>
    /// Set the input rotation for rotate the camera
    /// </summary>
    /// <param name="_inputAxis">The position delta received from the input manager</param>
    private void SetRotation(object _inputAxis)
    {
        if (m_target)
        {
            m_rotation = Vector2.zero;
            return;
        }

        m_rotation = (Vector2)_inputAxis;
    }

    /// <summary>
    /// Returns the forward vector of the Camera projected on an X-Z plane
    /// </summary>
    public Vector3 Forward()
    {
        return Vector3.ProjectOnPlane(m_camera.forward, Vector3.up);
    }

    /// <summary>
    /// Returns the right vector of the camera projected on an X-Z plane
    /// </summary>
    public Vector3 Right()
    {
        return Vector3.ProjectOnPlane(m_camera.right, Vector3.up);
    }

    /// <summary>
    /// Function called when the game goes to pause. &#xD;
    /// Manages the camera movement to the menu screen (Book).
    /// </summary>
    private void ToMenu(object _obj = null)
    {
        if (m_target)
        {
            m_lastTarget = m_target;
            UnlockTarget();
        }

        if (!m_isTransferring)
        {
            m_playCameraPosition.Copy(m_camera);
        }
        else
        {
            StopCoroutine(m_toggleRoutine);
        }
        m_menuCameraPosition.Copy(m_menuCameraTransformPosition);
        m_toggleRoutine = StartCoroutine(MoveCamTo(m_menuCameraPosition));
    }

    /// <summary>
    /// Function called when the game resumes from pause.
    /// </summary>
    private void ToPlay(object _obj = null)
    {
        if (m_lastTarget)
        {
            LockTarget(m_lastTarget);
        }

        StopCoroutine(m_toggleRoutine);
        m_toggleRoutine = StartCoroutine(MoveCamTo(m_playCameraPosition));
    }

    /// <summary>
    /// Coroutine used to move the camera from and to the menu screen.
    /// </summary>
    private IEnumerator MoveCamTo(Pivot _destination)
    {
        m_isTransferring = true;
        Pivot cameraOriginPosition = new Pivot(m_camera);
        float counter = 0.0f;

        while (counter <= m_menuTransitionTime)
        {
            counter += Time.deltaTime;
            float lerpValue = counter / m_menuTransitionTime;
            m_camera.Lerp(cameraOriginPosition, _destination, lerpValue);
            yield return new WaitForEndOfFrame();
        }
        
        m_isTransferring = false;
        m_camera.Copy(_destination);
    }

    /// <summary>
    /// Check if the camera is currently transferring to a new Position (Menu, GamePlay)
    /// </summary>
    public bool IsTransferring()
    {
        return m_isTransferring;
    }

    public GameObject GetTarget()
    {
        return m_target?.gameObject;
    }
}
