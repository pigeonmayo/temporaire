using System;
using UnityEngine;

public class BookMenu : MonoBehaviour
{
    [SerializeField] private GameObject m_bookModel     = default;
    [SerializeField] private Animator m_bookAnimator    = default;

    private void OnEnable()
    {
        Mushboi.Instance.EventManager.RegisterEvent(EventID.PauseGame,OpenBook);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.ResumeGame,CloseBook);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.TurnPage,TurnPage);
    }

    private void OnDisable()
    {
        if (Mushboi.m_isClosing)
            return;
        
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.PauseGame,OpenBook);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.ResumeGame,CloseBook);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.TurnPage,TurnPage);
    }

    private void OpenBook(object _obj = null)
    {
        m_bookModel.SetActive(true);
    }

    private void TurnPage(object _side)
    {
        const string TURN_PAGE = "TurnPage";

        if (m_bookModel.activeSelf)
            m_bookAnimator.SetTrigger(TURN_PAGE);
    }

    private void CloseBook(object _obj = null)
    {
        m_bookModel.SetActive(false);
    }
}
