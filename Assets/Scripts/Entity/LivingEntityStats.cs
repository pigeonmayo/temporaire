﻿
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Statistic of all creatures in the game.Inherit from <see cref="EntityStats"/>
/// </summary>
/// <seealso cref="PlayerStats"/>
/// <seealso cref="EnemyStats"/>
public class LivingEntityStats : EntityStats
{
    public int m_strength                   = default;
    public bool m_isHostile                 = default;
    public List<Element> m_weakness         = default;

    //[SerializeField] private EntityState m_startingState = default;
    //[SerializeField] private List<EntityState> m_state = new List<EntityState>();
}