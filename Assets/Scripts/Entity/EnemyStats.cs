﻿using UnityEngine;

/// <summary>
/// Base Statistic of all enemies creatures in the game. Inherit from <see cref="LivingEntityStats"/>
/// </summary>
/// <seealso cref="PlayerStats"/>
/// <seealso cref="EntityStats"/>
[CreateAssetMenu(fileName = "new Enemy", menuName = "EnemyStats")]
public class EnemyStats : LivingEntityStats
{
    
}
