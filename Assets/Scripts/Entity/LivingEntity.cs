﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base structure of all creatures in the game.  Inherit from <see cref="Entity"/>
/// </summary>
/// <seealso cref="Player"/> 
/// <seealso cref="Enemy"/> 
public class LivingEntity : Entity
{
    private LivingEntityStats LivingEntityStats     => m_stats as LivingEntityStats;
    public bool IsHostile                           => LivingEntityStats.m_isHostile;
    public int Strength                             => LivingEntityStats.m_strength;
    public List<Element> Weakness                   => LivingEntityStats.m_weakness;

    public override void TakeDamage(int _damage, Element _element = null)
    {
        if (_element && Weakness.Contains(_element))
            base.TakeDamage(_damage*2);
        else
            base.TakeDamage(_damage, _element);
    }

    protected override void Die()
    {
        if (m_stats.m_loots.Count > 0)
        {
            for (int i = 0; i < LivingEntityStats.m_nbLoot.RandomInt(); i++)
            {
                GameObject randomLoot = m_stats.m_loots[Random.Range(0, m_stats.m_loots.Count)];
                Loot loot = Mushboi.Instance.PoolingManager.SpawnItem(randomLoot).GetComponent<Loot>();
                loot.SetPosition(transform.position+Vector3.up);
                loot.SpawnLoot();
            }
        }
        
        base.Die();
    }
}
