﻿
/// <summary>
/// Base structure of all enemies in the game. Inherit from <see cref="LivingEntity"/>
/// </summary>
/// <seealso cref="Player"/> 
/// <seealso cref="Entity"/> 
public class Enemy : LivingEntity,ITarget
{
    private EnemyStats EnemyStats => m_stats as EnemyStats;
}
