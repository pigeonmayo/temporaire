﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base Statistic of all entity in the game (Creatures and Objects)
/// </summary>
/// <seealso cref="PlayerStats"/>
/// <seealso cref="EnemyStats"/>
public class EntityStats : ScriptableObject
{
    public string m_name                    = default;
    public bool m_isIndestructible          = default;
    public int m_maxHealth                  = default;
    public MinMax m_nbLoot                  = default;
    public List<GameObject> m_loots         = default;
}
