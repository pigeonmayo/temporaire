﻿using UnityEngine;

/// <summary>
/// Base structure of all entity in the game (Creatures and Object)
/// </summary>
/// <seealso cref="LivingEntity"/> 
/// <seealso cref="Player"/> 
/// <seealso cref="Enemy"/> 
public class Entity : MonoBehaviour
{
    [SerializeField] protected EntityStats m_stats          = default;
    [SerializeField] protected GameObject m_dyingEffect     = default;
    [SerializeField] protected GameObject m_hitEffect       = default;
    protected int m_currentHealth                           = default;
    private Animator m_animator                             = default;

    public int MaxHealth                                    => m_stats.m_maxHealth;
    public bool IsIndestructible                            => m_stats.m_isIndestructible;
    public bool IsDead                                      => m_currentHealth <= 0;
    public string Name                                      => m_stats.name;

    public virtual void Awake()
    {
        m_currentHealth = MaxHealth;
        m_animator = GetComponent<Animator>();
    }
  
    /// <summary>
    /// Entity receive heal and increase the health.
    /// </summary>
    /// <param name="_heal">Heal the entity receive</param>
    public virtual void Heal(int _heal)
    {
        m_currentHealth = Mathf.Clamp(m_currentHealth += _heal,0,MaxHealth);
    }

    /// <summary>
    /// Entity receive damage and reduce the health. After check if it die/destroy.
    /// </summary>
    /// <param name="_damage">Damage the entity receive</param>
    /// <param name="_element">The <see cref="Element"/> of the damage</param>
    public virtual void TakeDamage(int _damage, Element _element = null)
    {
        const string HIT_TRIGGER = "Hit";

        if (IsDead)
            return;
        
        if(m_hitEffect)
            Mushboi.Instance.PoolingManager.SpawnItem(m_hitEffect).SetPosition(transform.position);
        //TODO Play SFX

        if (!IsIndestructible)
        {
            m_currentHealth = Mathf.Clamp(m_currentHealth -= _damage, 0, MaxHealth);

            if (IsDead)
            {
                Die();
                return;
            }
        }

        m_animator?.SetTrigger(HIT_TRIGGER);
    }

    /// <summary>
    /// Apply all effect when the entity die/destroy.
    /// </summary>
    protected virtual void Die()
    {
        const string DEAD_BOOL = "Dead";
        
        if(m_dyingEffect)
            Mushboi.Instance.PoolingManager.SpawnItem(m_dyingEffect).SetPosition(transform.position);
        //TODO Play SFX
        
        GetComponent<Collider>().enabled = false;
        m_animator?.SetBool(DEAD_BOOL,true);
        Mushboi.Instance.EventManager.BroadcastEvent(EventID.EntityDie,this);
        Destroy(this);
    }
}
