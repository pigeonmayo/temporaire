using Unity.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const float VELOCITY_MULTIPLIER = 5f;

    [Header("Camera")]
    [SerializeField] private CameraController m_camController           = default;
    
    [Header("Movement")]
    [SerializeField] private float m_speed                              = 5f;
    [SerializeField] private float m_sprintingRatio                     = 2f;
    [SerializeField] private float m_rotationSpeed                      = 1f;
    private Vector3 m_currentMovement                                   = default;
    
    [Header("Jump")]
    [SerializeField] private float m_maxJumpHeight                      = 1f;
    [SerializeField] private float m_maxJumpTime                        = 0.5f;
    [SerializeField] private LayerMask m_groundMask                     = default;
    private float m_initJumpVelocity                                    = default;
    private float m_gravity                                             = default;
    private bool m_isJumpPressed                                        = default;
    
    private CharacterController m_characterController                   = default;
    private Player m_player                                             = default;
    private Vector2 m_controllerAxis                                    = Vector2.zero;
    private bool m_isGrounded                                           = default;
    private bool m_isJumping                                            = default;
    private bool m_isLocking                                            = default;
    private bool m_isSprinting                                          = default;
    private bool m_isGuarding                                           = default;

    //Getter
    public bool IsLocking                                               => m_isLocking;
    public bool IsGrounded                                              => m_isGrounded;
    public bool IsGuarding                                              => m_isGuarding;
    public bool IsSprinting                                             => m_isSprinting;
    
    private void OnEnable()
    {
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Defend,Defend);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Jump,PressJump);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Attack,Attack);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Interact,Interact);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Walk,Walk);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.Sprint,Sprint);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.LockTarget,LockTarget);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.CycleTarget,CycleTarget);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.ChangingElement,ChangingElement);
        Mushboi.Instance.EventManager.RegisterEvent(EventID.CastSpell,CastSpell);
    }
    
    private void OnDisable()
    {
        if (Mushboi.m_isClosing)
            return;

        Mushboi.Instance.EventManager.UnregisterEvent(EventID.Defend,Defend);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.Jump,PressJump);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.Attack,Attack);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.Interact,Interact);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.Walk,Walk);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.Sprint,Sprint);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.LockTarget,LockTarget);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.CycleTarget,CycleTarget);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.ChangingElement,ChangingElement);
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.CastSpell,CastSpell);
    }

    private void Awake()
    {
        m_player = GetComponent<Player>();
        m_characterController = GetComponent<CharacterController>();
        m_characterController.detectCollisions = true;

        float timeToApex = m_maxJumpTime / 2;
        m_gravity = (-2 * m_maxJumpHeight) / Mathf.Pow(timeToApex, 2);
        m_initJumpVelocity = (2 * m_maxJumpHeight) / timeToApex;
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        UpdateRotation();
        
        Move();
        m_isGrounded = CheckGround();
        
        UpdateGravity();
        UpdateJump();
    }

    private void UpdateGravity()
    {
        const float GROUNDED_GRAVITY = -0.5f;

        if (m_isGrounded)
            m_currentMovement.y = GROUNDED_GRAVITY;
        else
        {
            float previousYVelocity = m_currentMovement.y;
            float newYVelocity = m_currentMovement.y + (m_gravity * Time.deltaTime);
            float nextYVelocity = (previousYVelocity + newYVelocity) * 0.5f;
            m_currentMovement.y = nextYVelocity;
        }
    }

    private void UpdateRotation()
    {
        if (m_controllerAxis != Vector2.zero)
        {
            Vector3 positionToLookAt = new Vector3(m_currentMovement.x, 0, m_currentMovement.z);
            Quaternion currentRot = transform.rotation;
            Quaternion targetRot = Quaternion.LookRotation(positionToLookAt);
            transform.rotation = Quaternion.Slerp(currentRot, targetRot, m_rotationSpeed*Time.deltaTime);
        }
    }

    private void UpdateJump()
    {
        if (!m_isJumping && m_isGrounded && m_isJumpPressed)
            Jump();
        else if(!m_isJumpPressed && m_isJumping && m_isGrounded)
            m_isJumping = false;
    }
    
    private void LateUpdate()
    {
        m_camController.UpdateCameraRig(transform);
    }

    //TODO Redo Movement =================================
    /// <summary>
    /// Move the <see cref="Player"/> in the world with the axis changed in the <see cref="Walk"/> function.
    /// </summary>
    private void Move()
    {
        float y = m_currentMovement.y;
        float speed = (m_isSprinting ? m_speed * m_sprintingRatio : m_speed);
        
        m_currentMovement = (m_camController.Right() * m_controllerAxis.x  + 
                             m_camController.Forward() * m_controllerAxis.y).normalized * speed;
        m_currentMovement.y = y;
        
        m_characterController.Move(m_currentMovement * Time.deltaTime);
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a jumpInput. Give the <see cref="Player"/> a force to jump.
    /// </summary>
    /// <param name="_inputPressed">If the jump button is pressed</param>
    private void PressJump(object _inputPressed)
    {
        m_isJumpPressed = (bool)_inputPressed;
    }

    public void Jump()
    {
        m_isJumping = true;
        m_currentMovement.y = m_initJumpVelocity*0.5f;
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a sprintInput. Increase the <see cref="Player"/> speed.
    /// </summary>
    /// <param name="_inputPressed">If the jump button is pressed</param>
    private void Sprint(object _inputPressed)
    {
        m_isSprinting = (bool)_inputPressed;
    }
    
    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a moveInput. Set the movement vector of the <see cref="Player"/>.
    /// </summary>
    /// <param name="_controllerAxis">The axis (<see cref="Vector2"/>) of the movement input</param>
    private void Walk(object _controllerAxis)
    {
        m_controllerAxis = (Vector2)_controllerAxis;
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive an attackInput. Start a small or big attack animation.
    /// </summary>
    /// <param name="_inputPressed">If the jump button is pressed</param>
    private void Attack(object _inputPressed)
    {
        if (m_player.GetInteractable())
            return;
        
        if ((bool)_inputPressed)
        {
            
        }
        else
        {
            
        }
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive an interactInput. Called the interact function to the <see cref="IInteract"/> object.
    /// </summary>
    /// <param name="_inputValue">If the interact button is pressed</param>
    private void Interact(object _inputValue = null)
    {
        GameObject interactable = m_player.GetInteractable();
        
        if (!interactable)
            return;
        
        interactable.GetComponent<IInteract>().Interact();
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a defendInput. Block the attacks in front of the <see cref="Player"/>.
    /// </summary>
    /// <param name="_inputPressed">If the jump button is pressed</param>
    private void Defend(object _inputPressed)
    {
        m_isGuarding = (bool)_inputPressed;
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a lockInput. Lock the camera on the most center target on screen.
    /// The firefly will follow the target.
    /// </summary>
    private void LockTarget(object _inputValue = null)
    {
        if (!m_isLocking)
        {
            GameObject target = m_player.GetTarget();
            if (target)
            {
                Mushboi.Instance.CameraController.LockTarget(target);
                Mushboi.Instance.Firefly.NewTarget(target.transform);
                m_isLocking = true;
            }
        }
        else
        {
            m_isLocking = false;
            Mushboi.Instance.CameraController.UnlockTarget();
            Mushboi.Instance.Firefly.ReturnToPlayer();
        }
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a cycleTargetInput. Cycle through all <see cref="ITarget"/> entities in range.
    /// </summary>
    /// <param name="_inputValue">If the cycle is to the right or to the left with the value in float</param>
    private void CycleTarget(object _inputValue)
    {
        if (!m_isLocking)
            return;

        if ((float) _inputValue > 0)
        {
            GameObject nextTarget = m_player.NextTarget();
            Mushboi.Instance.CameraController.LockTarget(nextTarget);
            Mushboi.Instance.Firefly.NewTarget(nextTarget.transform);
        }
        else
        {
            GameObject previousTarget = m_player.PreviousTarget();
            Mushboi.Instance.CameraController.LockTarget(previousTarget);
            Mushboi.Instance.Firefly.NewTarget(previousTarget.transform);
        }
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a changingElementInput. Change the <see cref="Element"/> selected of the <see cref="Player"/>.
    /// Will change the <see cref="Element"/> of the <see cref="Firefly"/> too.
    /// </summary>
    /// <param name="_element">The new <see cref="Element"/></param>
    private void ChangingElement(object _element)
    {
        m_player.ChangeElement((Element)_element);
    }

    /// <summary>
    /// Called when the <see cref="InputManager"/> receive a castSpellInput. Cast the spell with the <see cref="Element"/> selected.
    /// </summary>
    /// <param name="_inputPressed">If the button is pressed</param>
    private void CastSpell(object _inputPressed)
    {
        bool isPressed = (bool) _inputPressed;
        Spell spell = m_player.GetSpell();
        
        if (isPressed)
            spell?.HoldSpell();
        else
            spell?.ReleaseSpell();
    }

    /// <summary>
    /// Check if the <see cref="Player"/> is grounded on the floor. A raycast is cast below is feet and detect all <see cref="Collider"/> in the ground layer.
    /// </summary>
    public bool CheckGround()
    {
        const float RANGE_RAYCAST = 1.3f;
        
        //return Physics.Raycast(transform.position+Vector3.up, Vector3.down, RANGE_RAYCAST, m_groundMask);
        return m_characterController.isGrounded;
    }
}
