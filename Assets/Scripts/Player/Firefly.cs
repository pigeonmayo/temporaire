using UnityEngine;
using UnityEngine.Serialization;

public class Firefly : MonoBehaviour
{
    [Header("Default Value")]
    [SerializeField] private Color m_defaultColor                                       = default;
    [SerializeField] private GameObject m_defaultVFX                                    = default;
    
    [Header("VFX & Color")]
    [SerializeField] private SerializableDictionary<Element, GameObject> m_vfxs         = default;
    [SerializeField] private Renderer[] m_colorRenderers                                = default;
    
    [FormerlySerializedAs("m_offset")]
    [Header("Stats")]
    [SerializeField] private Vector3 m_playerOffset                                     = default;
    [SerializeField,Range(0.1f,50f)] private float m_translateSpeed                     = default;
    private Element m_currentElement                                                    = default;
    private bool m_isOnTarget                                                           = false;
    private bool m_isOnPlayer                                                           = false;

    [Header("Reference")]
    [SerializeField] private GameObject m_model                                         = default;
    private Transform m_playerTransform                                                 = default;
    private Transform m_transformToFollow                                               = default;
    private Coroutine m_movingOffsetCoroutine                                           = default;
    private Coroutine m_movingCoroutine                                                 = default;
    
    private void Start()
    {
        Mushboi.Instance.SetFirefly(this);
        
        m_playerTransform = Mushboi.Instance.Player.transform;
        ReturnToPlayer();
        
        //Replace by animation
        m_model.transform.localPosition =  m_playerOffset;
        
        ChangeColor(m_defaultColor);
        ChangeVFX(m_defaultVFX);
    }

    private void LateUpdate()
    {
        if (m_isOnTarget || m_isOnPlayer)
        {
            transform.position = m_transformToFollow.position;
        }
    }

    /// <summary>
    /// Change the target to Follow. The <see cref="Firefly"/> will fly around this new target.
    /// </summary>
    /// <param name="_transform">The target to follow</param>
    public void NewTarget(Transform _transform)
    {
        StopAllCoroutines();

        m_isOnPlayer = false;
        m_isOnTarget = false;
        m_transformToFollow = _transform;
        float distance = Vector3.Distance(transform.position, _transform.position);
        float speed = distance / m_translateSpeed;
        Vector3 offset = new Vector3(0, _transform.GetComponent<Collider>().bounds.max.y - _transform.position.y, 0);
        
        m_movingOffsetCoroutine = StartCoroutine(m_model.transform.MoveToLocal(offset, speed));
        m_movingCoroutine = StartCoroutine(transform.MoveTo(m_transformToFollow, speed,() => {m_isOnTarget = true;}));
}

    public Transform GetTarget()
    {
        return m_isOnTarget?m_transformToFollow:null;
    }

    /// <summary>
    /// Reset the target to be <see cref="Player"/>. The <see cref="Firefly"/> will return next to the player.
    /// </summary>
    public void ReturnToPlayer()
    {
        StopAllCoroutines();
        
        m_isOnPlayer = false;
        m_isOnTarget = false;
        m_transformToFollow = m_playerTransform;
        float distance = Vector3.Distance(transform.position,m_playerTransform.position);
        float speed = distance / m_translateSpeed;
        
        m_movingOffsetCoroutine = StartCoroutine(m_model.transform.MoveToLocal(m_playerOffset, speed));
        m_movingCoroutine = StartCoroutine(transform.MoveTo(m_transformToFollow,speed, () => {m_isOnPlayer = true;}));
    }

    /// <summary>
    /// Change the <see cref="Element"/> of the <see cref="Firefly"/> to change color & vfx.
    /// </summary>
    /// <param name="_newElement">The new <see cref="Element"/></param>
    public void ChangeElement(Element _newElement)
    {
        m_currentElement = _newElement;
        ChangeColor(_newElement.m_elementColor);
        ChangeVFX(m_vfxs[_newElement]);
    }

    ///<summary>
    /// Change the VFX of the <see cref="Firefly"/> when the <see cref="Element"/> change.
    /// </summary>
    /// <param name="_newVFX">The new VFX</param>
    public void ChangeVFX(GameObject _newVFX)
    {
        foreach (SerializableDictionary<Element, GameObject>.Pair p in m_vfxs)
        {
            if(p.Value == _newVFX)
                p.Value.SetActive(true);
            else
                p.Value.SetActive(false);
        }
    }

    /// <summary>
    /// Change the <see cref="Color"/> of the <see cref="Material"/>s of the <see cref="Firefly"/>
    /// when the <see cref="Element"/> change.
    /// </summary>
    /// <param name="_color">The new <see cref="Color"/></param>
    public void ChangeColor(Color _color)
    {
        foreach (Renderer mat in m_colorRenderers)
        {
            mat.material.color = _color;
        } 
    }
}
