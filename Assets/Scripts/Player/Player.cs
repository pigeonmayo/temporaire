using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Structure of the <see cref="Player"/>. Inherit from <see cref="LivingEntity"/>
/// </summary>
/// <seealso cref="Enemy"/> 
/// <seealso cref="Entity"/> 
public class Player : LivingEntity
{
    [Header("Player Abilities")]
    [SerializeField] private SerializableDictionary<Element, Spell> m_spells    = default;
    [SerializeField] private List<Skill> m_skills                               = default;
    [SerializeField] private Transform m_shootingPoint                          = default;
    
    [Header("Player Stats")]
    [SerializeField,Range(0f,100f)] private float m_targetRange                 = default;
    [SerializeField,Range(0f,100f)] private float m_interactRange               = default;
    private PlayerController m_playerController                                 = default;
    private Sensor m_sensor                                                     = default;
    private Element m_currentElement                                            = default;
    private int m_currentMana                                                   = default;
    
    //[Header("Inventory")]
    private int m_money                                                         = default;
    
    //Getter
    public PlayerController PlayerController                                    => m_playerController;
    public PlayerStats PlayerStats                                              => m_stats as PlayerStats;
    public Element Element                                                      => m_currentElement;
    public int MaxMana                                                          => PlayerStats.m_maxMana;
    public int Money                                                            => PlayerStats.m_maxMana;
    //private Weapon Weapon                                                       => PlayerStats.m_weapon;
    //private Shield Shield                                                       => PlayerStats.m_shield;

    public override void Awake()
    {
        base.Awake();
        
        m_sensor = GetComponentInChildren<Sensor>();
        m_sensor.ChangeRange(m_targetRange,m_interactRange);

        m_playerController = GetComponent<PlayerController>();
        
        Mushboi.Instance.SetPlayer(this);
        
        InitSkills();
    }

    public void OnDisable()
    {
        foreach (Skill skill in m_skills)
        {
            skill?.Unload();   
        }
    }
    
    private void Update()
    {
        UpdateSkills();
        UpdateSpells();
    }

    /// <summary>
    /// Initialize all skill already on equipped on the player
    /// </summary>
    private void InitSkills()
    {
        foreach (Skill skill in m_skills)
        {
            skill?.Load();   
        }
    }
    
    /// <summary>
    /// Update all spells equipped by the player.
    /// </summary>
    private void UpdateSpells()
    {
        foreach (SerializableDictionary<Element,Spell>.Pair pair in m_spells)
        {
            pair.Value?.UpdateSpell();
        }
    }
    
    /// <summary>
    /// Update all skills equipped by the player.
    /// </summary>
    private void UpdateSkills()
    {
        foreach (Skill skill in m_skills)
        {
            skill?.UpdateSkill();
        }
    }

    /// <summary>
    /// Add a new <see cref="Skill"/> to the list.
    /// </summary>
    /// <param name="_skill">The new <see cref="Skill"/></param>
    public void ReceiveSkill(Skill _skill)
    {
        if (!m_skills.Contains(_skill))
        {
            m_skills.Add(_skill);
            _skill.Load();
        }
    }

    /// <summary>
    /// Change the <see cref="Element"/> selected of the <see cref="Player"/>
    /// </summary>
    /// <param name="_element">The new <see cref="Element"/> to switch</param>
    public void ChangeElement(Element _element)
    {
        if (m_spells.ContainsKey(_element))
        {
            if (m_spells[_element])
            {
                Mushboi.Instance.Firefly.ChangeElement(_element);

                m_currentElement = _element;
                Debug.Log($"Current Element is now : {m_currentElement.name}");
            }
        }
    }

    /// <summary>
    /// Add the new <see cref="Spell"/> to the <see cref="Player"/> list.
    /// </summary>
    /// <param name="_spell">the new <see cref="Spell"/></param>
    public void ReceiveSpell(Spell _spell)
    {
        m_spells.Add(new SerializableDictionary<Element, Spell>.Pair(_spell.m_element,_spell));
    }

    /// <summary>
    /// Replace the <see cref="Spell"/> with the same <see cref="Element"/> by the new <see cref="Spell"/>.
    /// </summary>
    /// <param name="_spell">The new <see cref="Spell"/></param>
    public void UpgradeSpell(Spell _spell)
    {
        m_spells[_spell.m_element] = _spell;
    }

    /// <summary>
    /// Return the <see cref="Spell"/> with the <see cref="Element"/> currently selected by the <see cref="Player"/>.
    /// </summary>
    public Spell GetSpell()
    {
        return m_currentElement ? m_spells[m_currentElement] : null;
    }

    public Vector3 GetShootingPosition()
    {
        return m_shootingPoint.position;
    }

    /// <summary>
    /// Get the <see cref="ITarget"/> closest to the center of screen
    /// </summary>
    /// <returns><see cref="GameObject"/> of the <see cref="ITarget"/></returns>
    public GameObject GetTarget()
    {
        return m_sensor.GetTargetable();
    }

    /// <summary>
    /// Get the <see cref="IInteract"/> closest to the <see cref="Player"/>
    /// </summary>
    /// <returns><see cref="GameObject"/> of the <see cref="IInteract"/></returns>
    public GameObject GetInteractable()
    {
        return m_sensor.GetInteractable();
    }

    /// <summary>
    /// Get the next <see cref="ITarget"/> from the list of <see cref="ITarget"/> in the player range.
    /// </summary>
    /// <returns><see cref="GameObject"/> of the <see cref="ITarget"/></returns>
    public GameObject NextTarget()
    {
        GameObject currentTarget = Mushboi.Instance.CameraController.GetTarget();
        return m_sensor.NextTargetable(currentTarget);
    }

    /// <summary>
    /// Get the previous <see cref="ITarget"/> from the list of <see cref="ITarget"/> in the player range.
    /// </summary>
    /// <returns><see cref="GameObject"/> of the <see cref="ITarget"/></returns>
    public GameObject PreviousTarget()
    {
        GameObject currentTarget = Mushboi.Instance.CameraController.GetTarget();
        return m_sensor.PreviousTargetable(currentTarget);
    }

    public void AddMoney(int _money)
    {
        m_money += _money;
    }
    
    public void RemoveMoney(int _money)
    {
        m_money = Mathf.Clamp(m_money-_money,0,9999);
    }
}
