﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Detect and add to a list all <see cref="ITarget"/> <see cref="Entity"/> close to the <see cref="Player"/>.
/// </summary>
public class Sensor : MonoBehaviour
{
    [SerializeField] private List<GameObject> m_targetablesDetected     = new List<GameObject>();
    [SerializeField] private List<GameObject> m_interactableDetected    = new List<GameObject>();
    private float m_interactRange                                       = default;

    private void OnEnable()
    {
        Mushboi.Instance.EventManager.RegisterEvent(EventID.EntityDie,EntityDie);
    }

    private void OnDisable()
    {
        if (Mushboi.m_isClosing) //if the game is closing and the game instance has been destroyed already, return.
            return;
        
        Mushboi.Instance.EventManager.UnregisterEvent(EventID.EntityDie,EntityDie);
    }

    private void Update()
    {
        CleanEmptySpace();
    }
    
    /// <summary>
    /// Clear the list of all null founds.
    /// </summary>
    private void CleanEmptySpace()
    {
        for (int i = m_targetablesDetected.Count-1; i >= 0; i--)
            if(m_targetablesDetected[i] == null)
                m_targetablesDetected.Remove(m_targetablesDetected[i]);
        
        for (int i = m_interactableDetected.Count-1; i >= 0; i--)
            if(m_interactableDetected[i] == null)
                m_interactableDetected.Remove(m_interactableDetected[i]);
    }
    
    /// <summary>
    /// Get the Next <see cref="ITarget"/> in the list.
    /// </summary>
    /// <param name="_targetable">The current <see cref="ITarget"/></param>
    /// <returns>The <see cref="GameObject"/> of the next <see cref="ITarget"/></returns>
    public GameObject NextTargetable(GameObject _targetable)
    {
        if (m_targetablesDetected.Count == 0)
            return null;
        
        int index = 0;
        if (m_targetablesDetected.Contains(_targetable))
        {
            for (int i = 0; i < m_targetablesDetected.Count; i++)
            {
                if (m_targetablesDetected[i] == _targetable)
                {
                    index = i + 1;
                    break;
                }
            }
        }

        if (index >= m_targetablesDetected.Count)
            index = 0;
        
        return m_targetablesDetected[index];
    }
    
    /// <summary>
    /// Get the Previous <see cref="ITarget"/> in the list.
    /// </summary>
    /// <param name="_targetable">The current <see cref="ITarget"/></param>
    /// <returns>The <see cref="GameObject"/> of the previous <see cref="ITarget"/></returns>
    public GameObject PreviousTargetable(GameObject _targetable)
    {
        if (m_targetablesDetected.Count == 0)
            return null;
        
        int index = 0;
        if (m_targetablesDetected.Contains(_targetable))
        {
            for (int i = 0; i < m_targetablesDetected.Count; i++)
            {
                if (m_targetablesDetected[i] == _targetable)
                {
                    index = i - 1;
                    break;
                }
            }
        }

        if (index < 0)
            index = m_targetablesDetected.Count-1;
        
        return m_targetablesDetected[index];
    }

    /// <summary>
    /// Found the closest <see cref="ITarget"/> to the center of the screen.
    /// </summary>
    /// <returns>The <see cref="GameObject"/> of the found <see cref="ITarget"/></returns>
    public GameObject GetTargetable()
    {
        float closestAngle = 999;
        GameObject target = default;

        Vector3 camPosition = Mushboi.Instance.CameraController.transform.position;
        Vector3 camForward = Mushboi.Instance.CameraController.Forward();

        foreach (GameObject o in m_targetablesDetected)
        {
            float angle = Mathf.Abs(Vector3.SignedAngle(camForward,o.transform.position-camPosition,Vector3.up));
            if (angle < closestAngle)
            {
                closestAngle = angle;
                target = o;
            } 
        }
        
        return target;
    }

    /// <summary>
    /// Found the closest <see cref="IInteract"/> to the <see cref="Player"/>.
    /// </summary>
    /// <returns>The <see cref="GameObject"/> of the found <see cref="IInteract"/></returns>
    public GameObject GetInteractable()
    {
        float closest = 999;
        GameObject interactable = default;
        
        foreach (GameObject o in m_interactableDetected)
        {
            float distance = Vector3.Distance(o.transform.position,transform.position);
            if (distance < closest)
            {
                closest = distance;
                interactable = o;
            } 
        }
        
        return interactable;
    }

    /// <summary>
    /// Change the range of the <see cref="SphereCollider"/> to detect <see cref="ITarget"/> <see cref="Entity"/>
    /// and the range to detect <see cref="IInteract"/> objects.
    /// </summary>
    public void ChangeRange(float _targetRange,float _interactRange)
    {
        GetComponent<SphereCollider>().radius = _targetRange;
        m_interactRange = _interactRange;
    }

    private void EntityDie(object _obj)
    {
        GameObject entity = ((Entity) _obj).gameObject;

        if (m_targetablesDetected.Contains(entity))
            m_targetablesDetected.Remove(entity);
        if (m_interactableDetected.Contains(entity))
            m_interactableDetected.Remove(entity);

        if (Mushboi.Instance.CameraController.GetTarget() == entity)
        {
            GameObject nextTarget = NextTargetable(entity);

            if (nextTarget)
            {
                Mushboi.Instance.Firefly.NewTarget(nextTarget.transform);
                Mushboi.Instance.CameraController.LockTarget(nextTarget);
            }
            else
            {
                Mushboi.Instance.Firefly.ReturnToPlayer();
                Mushboi.Instance.CameraController.UnlockTarget();
            }
        }
    }

    public void OnTriggerStay(Collider _collider)
    {
        ITarget target = _collider.GetComponent<ITarget>();
        IInteract interact = _collider.GetComponent<IInteract>();
        
        if (target != null)
        {
            if (!m_targetablesDetected.Contains(_collider.gameObject))
                m_targetablesDetected.Add(_collider.gameObject);
        }
        
        if (interact != null)
        {
            float distance = Vector3.Distance(transform.position,_collider.transform.position);
            if (!m_interactableDetected.Contains(_collider.gameObject) && distance <= m_interactRange)
            {
                m_interactableDetected.Add(_collider.gameObject);
            }
            else if (distance > m_interactRange)
            {
                if (m_interactableDetected.Contains(_collider.gameObject))
                    m_interactableDetected.Remove(_collider.gameObject);
            }
        }
    }

    public void OnTriggerExit(Collider _collider)
    {
        if (_collider.GetComponent<ITarget>() != null)
        {
            if (m_targetablesDetected.Contains(_collider.gameObject))
                m_targetablesDetected.Remove(_collider.gameObject);

            if(Mushboi.Instance.CameraController.GetTarget())
            {
                GameObject next = GetTargetable();
                if (!next)
                    Mushboi.Instance.EventManager.BroadcastEvent(EventID.LockTarget);
                else
                    Mushboi.Instance.CameraController.LockTarget(next);
            }
        }
        
        if (_collider.GetComponent<IInteract>() != null)
        {
            if (m_interactableDetected.Contains(_collider.gameObject))
                m_interactableDetected.Remove(_collider.gameObject);
        }
    }
}
