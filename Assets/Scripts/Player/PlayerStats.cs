using UnityEngine;

/// <summary>
/// Statistic of the player. Inherit from <see cref="LivingEntityStats"/>
/// </summary>
/// <seealso cref="EnemyStats"/>
/// <seealso cref="EntityStats"/>
[CreateAssetMenu(fileName = "Player", menuName = "PlayerStats")]
public class PlayerStats : LivingEntityStats
{
    public int m_maxMana                    = default;
}
