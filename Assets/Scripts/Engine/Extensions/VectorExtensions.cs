﻿
using UnityEngine;

public static class VectorExtensions
{
    public static Vector3 FlattenY(this Vector3 _vector)
    {
        return new Vector3(_vector.x,0,_vector.z);
    }

    public static Vector3 Half(this Vector3 _vector)
    {
        return _vector * 0.5f;
    }

    public static Vector3 Multiply(this Vector3 _a, Vector3 _b)
    {
        return new Vector3(_a.x * _b.x, _a.y * _b.y, _a.z * _b.z);
    }
}
