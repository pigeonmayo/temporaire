﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    /// <summary>
    /// Return a random item of the list
    /// </summary>
    public static T Random<T>(this IList<T> _list)
    {
        if (_list.Count == 0)
            throw new IndexOutOfRangeException("Cannot select a random item from an empty list !");
        
        return _list[UnityEngine.Random.Range(0,_list.Count)];
    }

    /// <summary>
    /// Remove all item of List B from list A
    /// </summary>
    /// <returns>Return the list A cleared</returns>
    public static List<T> RemoveList<T>(this List<T> _listA, List<T> _listB)
    {
        if (_listA.GetType() != _listB.GetType()) return null;
        
        foreach (T t in _listB)
            _listA.Remove(t);

        return _listA;
    }
    
    public static List<T> ChangeToComponentList<T>(this List<GameObject> _oldList)
    {
        List<T> newList = new List<T>();

        foreach (GameObject g in _oldList)
            newList.Add(g.GetComponent<T>());

        return newList;
    }
}