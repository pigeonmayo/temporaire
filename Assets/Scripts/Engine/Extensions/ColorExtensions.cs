﻿
using UnityEngine;

public static class ColorExtensions
{
    /// <summary>
    /// Return the Hexcode from a Unity Color
    /// </summary>
    public static string GetHexCode(this Color _color)
    {
        return "#" + ColorUtility.ToHtmlStringRGBA(_color);    
    }
}
