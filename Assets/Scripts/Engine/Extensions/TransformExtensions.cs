﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

public static class TransformExtensions
{
    /// <summary>
    /// Translate the gameobject from Point A to Point B in a given time and call an action at the end
    /// </summary>
    /// <param name="_target">Target Destination</param>
    /// <param name="_duration">Time to move the gameobject</param>
    /// <param name="_callback">The action to execute at the end</param>
    public static IEnumerator MoveTo(this Transform _transform, Vector3 _target, float _duration, Action _callback = null)
    {
        Vector3 startPos = _transform.position;
        float timer = 0;
        while (timer < _duration)
        {
            timer += Time.deltaTime;
            float lerpValue = timer / _duration;
            _transform.position = Vector3.Lerp(startPos, _target, lerpValue);
            yield return null;
        }
        _transform.position = _target;
        
        _callback?.Invoke();
    }
    
    /// <summary>
    /// Translate the gameobject from Point A to Point B in a given time and call an action at the end
    /// </summary>
    /// <param name="_target">Target Destination</param>
    /// <param name="_duration">Time to move the gameobject</param>
    /// <param name="_callback">The action to execute at the end</param>
    public static IEnumerator MoveToLocal(this Transform _transform, Vector3 _target, float _duration, Action _callback = null)
    {
        Vector3 startPos = _transform.localPosition;
        float timer = 0;
        while (timer < _duration)
        {
            timer += Time.deltaTime;
            float lerpValue = timer / _duration;
            _transform.localPosition = Vector3.Lerp(startPos, _target, lerpValue);
            yield return null;
        }
        _transform.localPosition = _target;
        
        _callback?.Invoke();
    }

    /// <summary>
    /// Translate the gameobject from Point A to Point B in a given time and call an action at the end
    /// </summary>
    /// <param name="_target">Target Destination</param>
    /// <param name="_duration">Time to move the gameobject</param>
    /// <param name="_callback">The action to execute at the end</param>
    public static IEnumerator MoveTo(this Transform _transform, Transform _target, float _duration, Action _callback = null)
    {
        Vector3 startPos = _transform.position;
        float timer = 0;
        while (timer < _duration)
        {
            timer += Time.deltaTime;
            float lerpValue = timer / _duration;
            _transform.position = Vector3.Lerp(startPos, _target.position, lerpValue);
            yield return null;
        }
        _transform.position = _target.position;
        
        _callback?.Invoke();
    }

    /// <summary>
    /// Remove all childrens of a transform
    /// </summary>
    public static void ClearChilds(this Transform _transform)
    {
        int nb = _transform.childCount;
        for (int i = nb - 1; i >= 0; i--)
            Object.Destroy(_transform.GetChild(i).gameObject);
    }
    
    /// <summary>
    /// Reset all transform information (Position,Rotation,Scale)
    /// </summary>
    public static void Reset(this Transform _transform)
    {
        _transform.localPosition = Vector3.zero;
        _transform.localRotation = Quaternion.identity;
        _transform.localScale = Vector3.one;
    }

    public static void Lerp(this Transform _transform, Pivot _origin, Pivot _destination, float _lerpValue)
    {
        _transform.position = Vector3.Lerp(_origin.position, _destination.position, _lerpValue);
        _transform.rotation = Quaternion.Lerp(_origin.rotation, _destination.rotation, _lerpValue);
    }
    
    public static void Lerp(this Transform _transform, Transform _origin, Transform _destination, float _lerpValue)
    {
        _transform.position = Vector3.Lerp(_origin.position, _destination.position, _lerpValue);
        _transform.rotation = Quaternion.Lerp(_origin.rotation, _destination.rotation, _lerpValue);
    }
    
    public static void Copy(this Transform _t, Transform _other)
    {
        _t.position = _other.position;
        _t.rotation = _other.rotation;
        _t.localScale = _other.localScale;
    }
    
    public static void Copy(this Transform _t, Pivot _other)
    {
        _t.position = _other.position;
        _t.rotation = _other.rotation;
        _t.localScale = _other.localScale;
    }
}
