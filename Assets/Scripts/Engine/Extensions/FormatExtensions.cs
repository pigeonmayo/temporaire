﻿
public static class FormatExtensions
{
    
    /// <summary>
    /// Return the time in second to Minutes:Seconds:Milliseconds
    /// </summary>
    /// <param name="_time">Time in Seconds</param>
    public static string FormatTime(this float _time)
    {
        int minutes = (int)_time / 60;
        int seconds = (int)_time - 60 * minutes;
        int milliseconds = (int)(1000 * (_time - minutes * 60 - seconds)) / 10;
        return $"{minutes:00}:{seconds:00}:{milliseconds:00}";
    }
}
