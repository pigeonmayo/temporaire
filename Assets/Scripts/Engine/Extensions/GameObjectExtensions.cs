﻿using UnityEngine;

public static class GameObjectExtensions
{
    /// <summary>
    /// Remove all childrens of a gameObject
    /// </summary>
    public static void ClearChilds(this GameObject _gameObject)
    {
        _gameObject.transform.ClearChilds();
    }
    
    /// <summary>
    /// Verify if the GameObject is in the layerMask
    /// </summary>
    /// <returns>True if the gameobject is in the layermask</returns>
    public static bool IsInLayerMask(this GameObject _obj, LayerMask _layerMask)
    {
        return ((_layerMask.value & (1 << _obj.layer)) > 0);
    }
}
