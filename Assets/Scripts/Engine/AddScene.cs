using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Add a <see cref="Scene"/> Async and switch the active <see cref="Scene"/> when the <see cref="Player"/> walk through a <see cref="Collider"/>.
/// </summary>
public class AddScene : MonoBehaviour
{
    [SerializeField] private SceneObject m_newScene                  = default;
    [SerializeField] private GameObject[] m_objectsToDisable    = default;
    
    private void OnTriggerEnter(Collider _other)
    {
        if (_other.transform.root.GetComponentInChildren<Player>())
        {
            if (Mushboi.Instance.ActivesMap.Contains(m_newScene))
                return;
            
            Transition.Instance.AddScene(m_newScene, () =>
            {
                Mushboi.Instance.AddActiveMap(m_newScene);
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(m_newScene));
                foreach (GameObject o in m_objectsToDisable)
                    o.SetActive(false);
            });
        }
    }
}
