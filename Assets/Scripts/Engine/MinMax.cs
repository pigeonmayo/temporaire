﻿using System;
using UnityEngine;

[Serializable]
public class MinMax
{
    [SerializeField, Range(-10000,10000)] private float m_min = 0;
    [SerializeField, Range(-10000,10000)] private float m_max = 0;

    public void OnValidate()
    {
        if (m_min > m_max)
        {
            (m_min, m_max) = (m_max, m_min);
        }
    }

    // ReSharper disable once InconsistentNaming
    public float min
    {
        get => m_min;

        set
        {
            if (value > m_min)
            {
                Debug.LogWarning($"Warning: min should be less than or equal to max! New MinMax : ({value}, {m_max})");
            }
            
            m_min = value;
        }
    }
    
    // ReSharper disable once InconsistentNaming
    public float max
    {
        get => m_max;
        
        set
        {
            if (value < m_min)
            {
                Debug.LogWarning($"Warning: max should be greater than or equal to min! New MinMax : ({m_min}, {value})");
            }
            
            m_max = value;
        }
    }

    /// <summary>
    /// Return a random (float)number between the minimum(Inclusive) and the maximum(Inclusive)
    /// </summary>
    public float Random()
    {
        return UnityEngine.Random.Range(min,max);
    }

    /// <summary>
    /// Return a random (int)number between the minimum(Inclusive) and the maximum(Inclusive)
    /// </summary>
    public int RandomInt()
    {
        return UnityEngine.Random.Range((int)min,(int)max+1);
    }
    
    public MinMax()
    {
        m_min = 0;
        m_max = 0;
    }
    
    public MinMax(float _a, float _b)
    {
        SetMinMax(_a, _b);
    }

    public void SetMinMax(float _a, float _b)
    {
        if (_a > _b)
        {
            //Tuple flip
            (_a, _b) = (_b, _a);
        }

        m_min = _a;
        m_max = _b;
    }
}
