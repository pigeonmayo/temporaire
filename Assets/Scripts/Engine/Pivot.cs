using UnityEngine;

/// <summary>
/// The pivot serves to represent a transform component.
/// Since a transform cannot be created as a new object alone, a pivot can store the properties of a transform
/// and serve as a kind of snapshot of the original transform.
/// </summary>
public class Pivot
{
    private Vector3 m_position;
    private Quaternion m_rotation;
    private Vector3 m_localPosition;
    private Quaternion m_localRotation;
    private Vector3 m_localScale;

    public Vector3 position => m_position;
    public Quaternion rotation => m_rotation;
    public Vector3 localPosition => m_localPosition;
    public Quaternion localRotation => m_localRotation;
    public Vector3 localScale => m_localScale;

    /// <summary>
    /// Builds a Pivot, passing the individual components separately. Use exclusively for unparented transforms.
    /// </summary>
    /// <param name="_pos">the position vector, serves as global and local pos</param>
    /// <param name="_rot">the rotation quaternion, serves as global and local rotation</param>
    /// <param name="_localScale">the scale vector</param>
    public Pivot(Vector3 _pos, Quaternion _rot, Vector3 _localScale)
    {
        m_position = _pos;
        m_rotation = _rot;
        m_localPosition = _pos;
        m_localRotation = _rot;
        
        m_localScale = _localScale;
    }

    /// <summary>
    /// Created a new Pivot using a Transform as a reference
    /// </summary>
    /// <param name="_base">The reference Transform.</param>
    public Pivot(Transform _base)
    {
        Copy(_base);
    }

    /// <summary>
    /// Creates a new Pivot using another Pivot as a reference
    /// </summary>
    /// <param name="_base">The reference Pivot</param>
    public Pivot(Pivot _base)
    {
        Copy(_base);
    }
    
    /// <summary>
    /// Copies the parameters of a Transform to this Pivot
    /// </summary>
    /// <param name="_other">The Transform to copy</param>
    public void Copy(Transform _other)
    {
        m_position = _other.position;
        m_rotation = _other.rotation;
        m_localPosition = _other.localPosition;
        m_localRotation = _other.localRotation;
        m_localScale = _other.localScale;
    }
    
    /// <summary>
    /// Copies the parameters of another Pivot to this one
    /// </summary>
    /// <param name="_other">The Pivot to copy</param>
    public void Copy(Pivot _other)
    {
        m_position = _other.position;
        m_rotation = _other.rotation;
        m_localPosition = _other.localPosition;
        m_localRotation = _other.localRotation;
        m_localScale = _other.localScale;
    }
}
