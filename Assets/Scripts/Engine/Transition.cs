﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Manage the transition between scenes. (Fade to black)
/// <example>
///     <code>Transition.ChangeScene("SomeScene")</code>
/// </example>
/// </summary>
public class Transition : Singleton<Transition>
{
    private const float MINIMUM_TIME             = 0f;
    private const float FADING_TIME              = 1f;
    
    private AsyncOperation m_loadSceneOperation  = default;

    [Header("Reference")]
    [SerializeField] private Image m_blocker     = default;

    /// <summary>
    /// Use to change between <see cref="Scene"/> with a fading transition.
    /// </summary>
    /// <param name="_sceneName">the name of the new <see cref="Scene"/></param>
    /// <param name="_onSceneChanged">Action to invoke when the new <see cref="Scene"/> is loaded.</param>
    /// <param name="_onLoadingProgress">Action to invoke while loading the new <see cref="Scene"/></param>
    public void ChangeScene(string _sceneName,Action _onSceneChanged = null,Action _onLoadingProgress = null)
    {
        m_loadSceneOperation = SceneManager.LoadSceneAsync(_sceneName);
        m_loadSceneOperation.allowSceneActivation = false;
        
        _onSceneChanged += AllowSceneChange;

        StartCoroutine(FadeChange(_onSceneChanged,_onLoadingProgress));
    }
    
    /// <summary>
    /// Allow the switch when the new <see cref="Scene"/> is loaded.
    /// </summary>
    private void AllowSceneChange()
    {
        m_loadSceneOperation.allowSceneActivation = true;
    }

    /// <summary>
    /// Fade to black before closing the game.
    /// </summary>
    public IEnumerator FadeQuit()
    {
        m_blocker.raycastTarget = true;

        float timer = 0;
        while (timer < FADING_TIME)
        {
            timer += Time.unscaledDeltaTime;
            
            float lerpValue = timer / FADING_TIME;
            m_blocker.color = Color.Lerp(Color.clear, Color.black, lerpValue);
            yield return null;  
        }
        
        Application.Quit();
    }

    /// <summary>
    /// Create a Fade without changing <see cref="Scene"/>.
    /// </summary>
    /// <param name="_action">Invoke the action in the middle of the transition</param>
    public IEnumerator FadeOnly(Action _action = null)
    {
        m_blocker.raycastTarget = true;

        float timer = 0;
        while (timer < FADING_TIME)
        {
            timer += Time.unscaledDeltaTime;
            
            float lerpValue = timer / FADING_TIME;
            m_blocker.color = Color.Lerp(Color.clear, Color.black, lerpValue);
            yield return null;  
        }
        
        _action?.Invoke();
        
        timer = 0;
        while (timer < FADING_TIME)
        {
            timer += Time.unscaledDeltaTime;
            
            float lerpValue = timer / FADING_TIME;
            m_blocker.color = Color.Lerp(Color.black, Color.clear, lerpValue);
            yield return null; 
        }
        
        m_blocker.raycastTarget = false;
    }
    
    private IEnumerator FadeChange(Action _onFadeDone,Action _onLoadingProgress = null)
    {
        m_blocker.raycastTarget = true;

        float timer = 0;
        while (timer < FADING_TIME)
        {
            timer += Time.unscaledDeltaTime;
            
            float lerpValue = timer / FADING_TIME;
            m_blocker.color = Color.Lerp(Color.clear, Color.black, lerpValue);
            yield return null;  
        }
        
        _onFadeDone.Invoke();
        //Minimum Loading Time
        yield return new WaitForSeconds(MINIMUM_TIME);
        
        while (!m_loadSceneOperation.isDone)
        {
            _onLoadingProgress?.Invoke();
            yield return null;
        }

        timer = 0;
        while (timer < FADING_TIME)
        {
            timer += Time.unscaledDeltaTime;
            
            float lerpValue = timer / FADING_TIME;
            m_blocker.color = Color.Lerp(Color.black, Color.clear, lerpValue);
            yield return null; 
        }
        
        m_blocker.raycastTarget = false;
    }

    /// <summary>
    /// Add a <see cref="Scene"/> in the game Async without pausing the game.
    /// </summary>
    /// <param name="_sceneName">The name of the <see cref="Scene"/> to be add.</param>
    /// <param name="_onLoadedScene">The <see cref="Action"/> to invoke when the <see cref="Scene"/> will be loaded.</param>
    public void AddScene(string _sceneName,Action _onLoadedScene = null)
    {
        m_loadSceneOperation = SceneManager.LoadSceneAsync(_sceneName,LoadSceneMode.Additive);
        StartCoroutine(AddScene(_onLoadedScene));
    }

    private IEnumerator AddScene(Action _onLoadedScene)
    {
        while (!m_loadSceneOperation.isDone)
            yield return null;

        _onLoadedScene?.Invoke();
    }

}
