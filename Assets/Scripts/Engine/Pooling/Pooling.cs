﻿using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour
{
    [SerializeField] private PoolItem m_prefab = default;
    [SerializeField, Range(0, 100)] protected int m_defaultSize = 0;
    protected List<PoolItem> m_actives = new List<PoolItem>();
    protected List<PoolItem> m_inactives = new List<PoolItem>();

    protected void Awake()
    {
        for (int i = 0; i < m_defaultSize; i++)
            AddToPool();
    }
    
    /// <summary>
    /// Get the <see cref="PoolItem"/> of the pooling
    /// </summary>
    /// <param name="_activated"></param>
    /// <returns></returns>
    public PoolItem GetPoolItem(bool _activated = true)
    {
        PoolItem item = m_inactives.Find(poolItem => poolItem);
        if (!item)
            item = AddToPool();

        m_inactives.Remove(item);
        m_actives.Add(item);
        if (_activated)
            item.Activate();

        return item;
    }

    private PoolItem AddToPool()
    {
        PoolItem item = Instantiate(m_prefab, transform);
        item.AssignRemove(OnRemoveCallback);

        return item;
    }

    private void OnRemoveCallback(PoolItem _item)
    {
        m_actives.Remove(_item);
        m_inactives.Add(_item);
    }

    public void SetPoolItem(PoolItem _poolItem)
    {
        m_prefab = _poolItem;
    }

    public void DesactiveAll()
    {
        PoolItem[] items = m_actives.ToArray();
        foreach (PoolItem poolItem in items)
            poolItem.Deactivate();
    }
}
