﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Manage the <see cref="GameObject"/> in a <see cref="Pooling"/> list.
/// Caution: To delete an <see cref="PoolItem"/> use <see cref="Deactivate"/>
/// </summary>
/// <seealso cref="PoolingManager"/>
public class PoolItem : MonoBehaviour
{
    private Action<PoolItem> m_onRemove = default;
    
    /// <summary>
    /// Set the Action to be invoked when the <see cref="PoolItem"/> will be delete.
    /// </summary>
    /// <param name="_callback"></param>
    public void AssignRemove(Action<PoolItem> _callback)
    {
        m_onRemove += _callback;
        Deactivate();
    }
    
    /// <summary>
    /// Change to position of a <see cref="PoolItem"/>.
    /// </summary>
    public void SetPosition(Vector3 _position)
    {
        transform.position = _position;
    }
    
    public virtual void Activate()
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Destroy the <see cref="PoolItem"/>.
    /// </summary>
    /// <param name="_timer">The delay to destroy this <see cref="PoolItem"/></param>
    public void Deactivate(float _timer = 0)
    {
        if (_timer != 0)
        {
            StartCoroutine(DeactivateDelay(_timer));
            return;
        }

        m_onRemove?.Invoke(this);
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Destroy the <see cref="PoolItem"/> with a delay.
    /// </summary>
    /// <param name="_timer">The delay to destroy this <see cref="PoolItem"/></param>
    private IEnumerator DeactivateDelay(float _timer)
    {
        yield return new WaitForSeconds(_timer);

        m_onRemove?.Invoke(this);
        gameObject.SetActive(false);
    }

    public void OnParticleSystemStopped()
    {
        Deactivate();
    }
}
