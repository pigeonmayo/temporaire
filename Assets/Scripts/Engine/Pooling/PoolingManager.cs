﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manage <see cref="Pooling"/> with all <see cref="PoolItem"/>
/// Create <see cref="Pooling"/> if no <see cref="Pooling"/> exist of this <see cref="PoolItem"/>
///     <para>Use this for Spawning an <see cref="PoolItem"/></para>
///     <example>
///         <code>Mushboi.Instance.PoolingManager.SpawnItem(someGameobject);</code>
///     </example>
/// </summary>
public class PoolingManager : MonoBehaviour
{
    private Dictionary<string,Pooling> m_poolings = new Dictionary<string,Pooling>();

    /// <summary>
    /// Spawn a copie of the object in a <see cref="Pooling"/> even if no pooling is found. It'll created one automatically.
    /// </summary>
    /// <param name="_gameObject">The prefab to spawn</param>
    /// <returns>Return the created <see cref="GameObject"/></returns>
    public PoolItem SpawnItem(GameObject _gameObject)
    {
        if (!m_poolings.ContainsKey(_gameObject.name))
            if (!CreatePooling(_gameObject))
                return null;

        return m_poolings[_gameObject.name].GetPoolItem();
    }

    /// <summary>
    /// Create a pooling of the given <see cref="GameObject"/>
    /// </summary>
    /// <param name="_gameObject">The <see cref="GameObject"/> who will be spawned in the new <see cref="Pooling"/></param> 
    private bool CreatePooling(GameObject _gameObject)
    {
        PoolItem poolItem = _gameObject.GetComponent<PoolItem>();
        if (!poolItem)
        {
            return false;
        }

        GameObject poolingObject = new GameObject($"Pooling {poolItem.name}");
        poolingObject.transform.parent = transform;
        
        Pooling pooling = poolingObject.AddComponent<Pooling>();
        Debug.Log($"Pooling [{poolItem.name}] created");
        pooling.SetPoolItem(poolItem);
        
        m_poolings.Add(_gameObject.name,pooling);

        return true;
    }
}