﻿using UnityEngine;
using UnityEngine.Events;

public class AnimationEvent : MonoBehaviour
{
    [SerializeField] private SerializableDictionary<string,UnityEvent> m_actions = default;
    
    public void ExecuteAction(string _key)
    {
        m_actions[_key]?.Invoke();
    }

    public new void DestroyObject(Object _obj)
    {
        Destroy(_obj);
    }
}
