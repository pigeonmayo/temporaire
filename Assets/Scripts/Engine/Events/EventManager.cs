﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum EventID
{                    
    SaveGame,
    LoadGame,
    PauseGame,
    ResumeGame,
    RotateCamera,
    Walk,
    Swim,
    Jump,
    Attack,
    Defend,
    Interact,
    LockTarget,
    Sprint,
    CycleTarget,
    ChangingElement,
    CastSpell,
    GroundPound,
    LookForward,
    TurnPage,
    EntityDie,
}

public class EventManager : MonoBehaviour
{
    private Dictionary<EventID, Action<object>> m_eventDict = new Dictionary<EventID, Action<object>>();

    /// <summary>
    /// To register an action to a Event
    /// </summary>
    /// <param name="_key">The event who will can the action</param>
    /// <param name="_callback">The action who will execute when the event is trigger</param>
    public void RegisterEvent(EventID _key,Action<object> _callback)
    {
        if (m_eventDict.ContainsKey(_key))
        {
            m_eventDict[_key] += _callback;
        }
        else
        {
            m_eventDict.Add(_key,_callback);
        }
    }

    /// <summary>
    /// To Unregister an action to a Event
    /// </summary>
    /// <param name="_key">The event who will can the action</param>
    /// <param name="_callback">The action who will execute when the event is trigger</param>
    public void UnregisterEvent(EventID _key,Action<object> _callback)
    {
        if (m_eventDict.ContainsKey(_key))
        {
            m_eventDict[_key] -= _callback;
        }
    }

    /// <summary>
    /// Invoke all actions registered to the _key event
    /// </summary>
    /// <param name="_key">The EventID to Invoke</param>
    /// <param name="_param">The parameter to be sent to all actions registered to the EventID</param>
    public void BroadcastEvent(EventID _key,object _param = null)
    {
        if (m_eventDict.ContainsKey(_key))
        {
            m_eventDict[_key]?.Invoke(_param);
        }
        else
        {
            Debug.LogWarning("The EventID isn't register");
        }
    }
}
