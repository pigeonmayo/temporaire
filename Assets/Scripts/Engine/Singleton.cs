﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    [SerializeField] private bool m_dontDestroy  = default;
    public static bool m_isClosing               = false;
    private static T m_instance                  = default;
    public static T Instance
    {
        get
        {
            if (!m_instance)
                m_instance = Instantiate(Resources.Load<T>($"{typeof(T)}"));

            return m_instance;
        }
    }

    private void OnDestroy()
    {
        m_isClosing = true;
    }
    
    public virtual void Awake()
    {
        if (m_instance)
            Destroy(gameObject);
        else
        {
            m_instance = GetComponent<T>();
            if (m_dontDestroy)
            {
                DontDestroyOnLoad(gameObject); 
                foreach (Transform child in transform)
                {
                    DontDestroyOnLoad(child.gameObject); 
                }
            }
        }
    }
    
    public void ForceInit(){}
}
