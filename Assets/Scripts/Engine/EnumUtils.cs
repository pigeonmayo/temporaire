﻿using System;

/// <summary>
/// Regroup all functions created to bve useful with the enums.
/// </summary>
public static class EnumUtils
{
    /// <summary>
    /// Return the length of the <see cref="Enum"/>
    /// </summary>
    public static int GetEnumLength(this Enum _type)
    {
        return Enum.GetNames(_type.GetType()).Length;
    }
}