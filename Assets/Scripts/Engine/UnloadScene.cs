﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Unload an added scene.
/// </summary>
public class UnloadScene : MonoBehaviour
{
    [SerializeField] private SceneObject m_oldScene = default;

    private void OnTriggerEnter(Collider _other)
    {
        if (_other.transform.root.GetComponentInChildren<Player>())
        {
            Mushboi.Instance.RemoveActiveMap(m_oldScene);
            SceneManager.UnloadSceneAsync(m_oldScene);
        }
    }
}