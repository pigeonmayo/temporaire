using UnityEngine;

public class AOE : PoolItem
{
    private SerializableDictionary<Entity, float> m_entities                    = new SerializableDictionary<Entity, float>();
    private float m_damageTime                                                  = 0.5f;

    private int m_damage                                                        = default;
    private Element m_element                                                   = default;
    private LayerMask m_layerMask                                               = default;
    private Transform m_toFollow                                                = default;
    
    public void SetAOE(int _damage,float _lifeTime,Element _element,LayerMask _layerMask,Transform _toFollow = null)
    {
        m_damage = _damage;
        m_element = _element;
        m_layerMask = _layerMask;
        m_toFollow = _toFollow;
        
        Deactivate(_lifeTime);
    }

    private void Update()
    {
        if (m_toFollow)
            transform.position = m_toFollow.position;
        
        foreach (var pair in m_entities)
        {
            m_entities[pair.Key] -= Time.deltaTime;
            
            if (pair.Value <= 0f)
            {
                m_entities[pair.Key] = m_damageTime;
                pair.Key.TakeDamage(m_damage);
            }
        }
    }

    private void OnTriggerStay(Collider _other)
    {
        Entity entity = _other.transform.GetComponentInChildren<Entity>();
        if (!entity || m_entities.ContainsKey(entity))
            return;
        
        m_entities.Add(new SerializableDictionary<Entity, float>.Pair(entity,m_damageTime));
    }

    private void OnTriggerExit(Collider _other)
    {
        Entity entity =  _other.transform.GetComponentInChildren<Entity>();
        if (!entity || !m_entities.ContainsKey(entity))
            return;

        m_entities.Remove(entity);
    }
}
