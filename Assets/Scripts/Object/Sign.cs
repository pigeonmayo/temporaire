using UnityEngine;

public class Sign : MonoBehaviour, ITarget, IInteract
{
    [SerializeField,TextArea] private string m_text = default;

    public void Interact()
    {
        Debug.Log($"Interact with {name}: \n {m_text}");
    }
}
