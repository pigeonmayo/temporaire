using UnityEngine;

public class Projectile : PoolItem
{
    private int m_damage            = default;
    private Element m_element       = default;
    private Rigidbody m_rigidbody   = default;
    private LayerMask m_layerMask   = default;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    public void SetProjectile(int _damage,float _lifeTime,Element _element,Vector3 _force,LayerMask _layerMask)
    {
        m_damage = _damage;
        m_element = _element;
        m_layerMask = _layerMask;
        
        m_rigidbody.velocity = Vector3.zero;
        m_rigidbody.AddForce(_force,ForceMode.VelocityChange);
        
        Deactivate(_lifeTime);
    }

    private void OnTriggerEnter(Collider _other)
    {
        if (_other.gameObject.IsInLayerMask(m_layerMask))
        {
            if (_other.TryGetComponent(out Entity entity))
                entity.TakeDamage(m_damage,m_element);

            Deactivate();
        }
    }
}
