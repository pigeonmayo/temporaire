using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour,IInteract
{
    [SerializeField] private SceneObject m_sceneToLoad  = default;
    private Animator m_animator                         = default;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    public void Interact()
    {
        const string OPEN_TRIGGER = "Open";
        m_animator.SetTrigger(OPEN_TRIGGER);
    }
    
    /// <summary>
    /// Called in the <see cref="Animator"/> when the open <see cref="Animation"/> is done.
    /// Will load the <see cref="Scene"/> for the other side. 
    /// </summary>
    public void OnDoorOpen()
    {
        Transition.Instance.ChangeScene(m_sceneToLoad);
    }
}
